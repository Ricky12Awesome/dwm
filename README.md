dwm - dynamic window manager
============================
dwm is an extremely fast, small, and dynamic window manager for X.

#### This is my own build of dwn, all patches used are located in `./patches`
you can configure this to your own liking by editing `config.def.h`

Patches
-------
* [alpha](https://dwm.suckless.org/patches/alpha/) - 
adds transparency (requires a compositor like `picom`)
* [anybar](https://dwm.suckless.org/patches/anybar/) - 
support for other bars like `polybar`
* [statuscmd](https://dwm.suckless.org/patches/statuscmd/) - 
ability to click on the status bar to execute commands
* ~~[status2d](https://dwm.suckless.org/patches/status2d/) - 
ability to use colors in status bar~~
Removed because if you have an incorrect format, it would crash dwm.
* [autostart](https://dwm.suckless.org/patches/autostart/) - 
automatically start applications (like `picom` for example)
* [cfacts](https://dwm.suckless.org/patches/cfacts/) - 
ability to resize windows
* [vanitygaps](https://dwm.suckless.org/patches/vanitygaps/) - 
adds gaps and some layouts
* [cyclelayouts](https://dwm.suckless.org/patches/cyclelayouts/) - 
ability to cycle layouts
* [ipc](https://dwm.suckless.org/patches/ipc/) - 
ability to send commands to dwm using `dwm-msg`
* [swallow](https://dwm.suckless.org/patches/swallow/) - 
ability to let your terminal swallow terminals 

Requirements
------------
In order to build dwm you need the Xlib header files.


Installation
------------
```sh
git clone https://gitlab.com/Ricky12Awesome/dwm
cd dwm
./install.sh
```

Edit config.mk to match your local setup (dwm is installed into
the /usr/local namespace by default).

Running dwm
-----------
Add the following line to your .xinitrc to start dwm using startx:

    exec dwm

In order to connect dwm to a specific display, make sure that
the DISPLAY environment variable is set correctly, e.g.:

    DISPLAY=foo.bar:1 exec dwm

(This will start dwm on display :1 of the host foo.bar.)

In order to display status info in the bar, you can do something
like this in your .xinitrc:

    while xsetroot -name "`date` `uptime | sed 's/.*,//'`"
    do
    	sleep 1
    done &
    exec dwm


Configuration
-------------
The configuration of dwm is done by creating a custom config.h
and (re)compiling the source code.
